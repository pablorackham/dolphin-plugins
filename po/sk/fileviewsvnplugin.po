# translation of fileviewsvnplugin.po to Slovak
# Richard Fric <Richard.Fric@kdemail.net>, 2010.
# Roman Paholik <wizzardsk@gmail.com>, 2015.
# Dušan Kazik <prescott66@gmail.com>, 2020.
# Matej Mrenica <matejm98mthw@gmail.com>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: fileviewsvnplugin\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-07-21 00:52+0000\n"
"PO-Revision-Date: 2020-11-07 13:04+0100\n"
"Last-Translator: Matej Mrenica <matejm98mthw@gmail.com>\n"
"Language-Team: Slovak <kde-i18n-doc@kde.org>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 20.08.3\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: fileviewsvnplugin.cpp:71
#, kde-format
msgctxt "@item:inmenu"
msgid "SVN Update"
msgstr "Aktualizovať SVN"

#: fileviewsvnplugin.cpp:77
#, kde-format
msgctxt "@item:inmenu"
msgid "Show Local SVN Changes"
msgstr "Zobraziť miestne zmeny SVN"

#: fileviewsvnplugin.cpp:83
#, kde-format
msgctxt "@item:inmenu"
msgid "SVN Commit..."
msgstr "Začleniť do SVN..."

#: fileviewsvnplugin.cpp:89
#, kde-format
msgctxt "@item:inmenu"
msgid "SVN Add"
msgstr "Pridať do SVN"

#: fileviewsvnplugin.cpp:95
#, kde-format
msgctxt "@item:inmenu"
msgid "SVN Delete"
msgstr "Odstrániť z SVN"

#: fileviewsvnplugin.cpp:101
#, kde-format
msgctxt "@item:inmenu"
msgid "SVN Revert"
msgstr "Vrátiť späť z SVN"

#: fileviewsvnplugin.cpp:107
#, kde-format
msgctxt "@item:inmenu"
msgid "Show SVN Updates"
msgstr "Zobraziť aktualizácie SVN"

#: fileviewsvnplugin.cpp:115
#, kde-format
msgctxt "@action:inmenu"
msgid "SVN Log..."
msgstr "Záznam SVN..."

#: fileviewsvnplugin.cpp:120
#, kde-format
msgctxt "@action:inmenu"
msgid "SVN Checkout..."
msgstr "SVN Checkout..."

#: fileviewsvnplugin.cpp:125
#, kde-format
msgctxt "@action:inmenu"
msgid "SVN Cleanup..."
msgstr "Vyčistiť SVN..."

#: fileviewsvnplugin.cpp:220
#, kde-format
msgctxt "@info:status"
msgid "SVN status update failed. Disabling Option \"Show SVN Updates\"."
msgstr ""
"Aktualizácia stavu SVN zlyhala. Zakazuje sa voľba \"Zobraziť aktualizácie SVN"
"\"."

#: fileviewsvnplugin.cpp:356
#, kde-format
msgctxt "@title:window"
msgid "SVN Update"
msgstr "Aktualizovať z SVN"

#: fileviewsvnplugin.cpp:360
#, kde-format
msgctxt "@info:status"
msgid "Updating SVN repository..."
msgstr "Aktualizuje sa repozitár SVN..."

#: fileviewsvnplugin.cpp:361
#, kde-format
msgctxt "@info:status"
msgid "Update of SVN repository failed."
msgstr "Aktualizácia repozitára SVN zlyhala."

#: fileviewsvnplugin.cpp:362
#, kde-format
msgctxt "@info:status"
msgid "Updated SVN repository."
msgstr "Repozitár SVN aktualizovaný."

#: fileviewsvnplugin.cpp:376
#, kde-format
msgctxt "@info:status"
msgid "Could not show local SVN changes."
msgstr "Nepodarilo sa zobraziť miestne zmeny SVN."

#: fileviewsvnplugin.cpp:391
#, kde-format
msgctxt "@info:status"
msgid "Could not show local SVN changes: svn diff failed."
msgstr "Nepodarilo sa zobraziť miestne zmeny SVN: rozdiel svn zlyhal."

#: fileviewsvnplugin.cpp:403 fileviewsvnplugin.cpp:587
#: fileviewsvnplugin.cpp:616
#, kde-format
msgctxt "@info:status"
msgid "Could not show local SVN changes: could not start kompare."
msgstr ""
"Nepodarilo sa zobraziť zmeny SVN: nepodarilo sa spustiť aplikáciu kompare."

#: fileviewsvnplugin.cpp:435
#, kde-format
msgctxt "@info:status"
msgid "Adding files to SVN repository..."
msgstr "Pridávanie súborov do repozitára SVN..."

#: fileviewsvnplugin.cpp:436
#, kde-format
msgctxt "@info:status"
msgid "Adding of files to SVN repository failed."
msgstr "Pridanie súborov do repozitára SVN zlyhalo."

#: fileviewsvnplugin.cpp:437
#, kde-format
msgctxt "@info:status"
msgid "Added files to SVN repository."
msgstr "Súbory pridané so repozitára SVN."

#: fileviewsvnplugin.cpp:443
#, kde-format
msgctxt "@info:status"
msgid "Removing files from SVN repository..."
msgstr "Odstraňujú sa súbory z repozitára SVN..."

#: fileviewsvnplugin.cpp:444
#, kde-format
msgctxt "@info:status"
msgid "Removing of files from SVN repository failed."
msgstr "Odstrňujú sa súbory z repozitára SVN zlyhalo."

#: fileviewsvnplugin.cpp:445
#, kde-format
msgctxt "@info:status"
msgid "Removed files from SVN repository."
msgstr "Súbory odstránené z repozitára SVN."

#: fileviewsvnplugin.cpp:465 fileviewsvnplugin.cpp:551
#, kde-format
msgctxt "@title:window"
msgid "SVN Revert"
msgstr "Vrátiť späť z SVN"

#: fileviewsvnplugin.cpp:469
#, kde-format
msgctxt "@info:status"
msgid "Reverting files from SVN repository..."
msgstr "Vracajú sa súbory z repozitára SVN..."

#: fileviewsvnplugin.cpp:470
#, kde-format
msgctxt "@info:status"
msgid "Reverting of files from SVN repository failed."
msgstr "Vrátenie súborov z repozitára SVN zlyhalo."

#: fileviewsvnplugin.cpp:471
#, kde-format
msgctxt "@info:status"
msgid "Reverted files from SVN repository."
msgstr "Súbory vrátené z repozitára SVN."

#: fileviewsvnplugin.cpp:555
#, kde-format
msgctxt "@info:status"
msgid "Reverting changes to file..."
msgstr "Vracajú sa zmeny súboru..."

#: fileviewsvnplugin.cpp:556
#, kde-format
msgctxt "@info:status"
msgid "Revert file failed."
msgstr "Vrátenie súboru späť zlyhalo."

#: fileviewsvnplugin.cpp:557
#, kde-format
msgctxt "@info:status"
msgid "File reverted."
msgstr "Súbor vrátený späť."

#: fileviewsvnplugin.cpp:574 fileviewsvnplugin.cpp:597
#: fileviewsvnplugin.cpp:602
#, kde-format
msgctxt "@info:status"
msgid "Could not show local SVN changes for a file: could not get file."
msgstr ""
"Nepodarilo sa zobraziť miestne zmeny súboru SVN: nepodarilo sa získať súbor."

#: fileviewsvnplugin.cpp:643 fileviewsvnplugin.cpp:666
#, kde-format
msgctxt "@info:status"
msgid "Commit of SVN changes failed."
msgstr "Začlenenie zmien do SVN zlyhalo."

#: fileviewsvnplugin.cpp:661 svncommitdialog.cpp:154
#, kde-format
msgctxt "@title:window"
msgid "SVN Commit"
msgstr "Začlenenie do SVN"

#: fileviewsvnplugin.cpp:665
#, kde-format
msgctxt "@info:status"
msgid "Committing SVN changes..."
msgstr "Začleňujú sa zmeny do SVN..."

#: fileviewsvnplugin.cpp:667
#, kde-format
msgctxt "@info:status"
msgid "Committed SVN changes."
msgstr "Zmeny SVN začlenené."

#. i18n: ectx: label, entry (showUpdates), group (General)
#: fileviewsvnpluginsettings.kcfg:9
#, kde-format
msgid "Show updates"
msgstr "Zobraziť aktualizácie"

#: svncheckoutdialog.cpp:56
#, kde-format
msgctxt "@title:window"
msgid "Choose a directory to checkout"
msgstr "Vyberte priečinok na checkout"

#: svncheckoutdialog.cpp:102
#, kde-format
msgctxt "@info:status"
msgid "SVN checkout: checkout in process..."
msgstr "SVN checkout: checkout prebieha..."

#: svncheckoutdialog.cpp:105
#, kde-format
msgctxt "@info:status"
msgid "SVN checkout: checkout failed."
msgstr "SVN checkout: checkout zlyhal."

#: svncheckoutdialog.cpp:107
#, kde-format
msgctxt "@info:status"
msgid "SVN checkout: checkout successful."
msgstr "SVN checkout: checkout úspešný."

#. i18n: ectx: property (windowTitle), widget (QWidget, SvnCheckoutDialog)
#: svncheckoutdialog.ui:20
#, kde-format
msgid "SVN Checkout"
msgstr "SVN Checkout"

#. i18n: ectx: property (text), widget (QLabel, label)
#: svncheckoutdialog.ui:26
#, kde-format
msgid "URL of repository:"
msgstr "URL repozitára:"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: svncheckoutdialog.ui:36
#, kde-format
msgid "Checkout directory:"
msgstr "Priečinok na checkout: "

#. i18n: ectx: property (text), widget (QCheckBox, cbOmitExternals)
#: svncheckoutdialog.ui:46
#, kde-format
msgid "Omit externals"
msgstr "Vynechať externé"

#. i18n: ectx: property (text), widget (QPushButton, pbOk)
#. i18n: ectx: property (text), widget (QPushButton, buttonOk)
#: svncheckoutdialog.ui:71 svncleanupdialog.ui:80 svnlogdialog.ui:146
#: svnprogressdialog.ui:46
#, kde-format
msgid "OK"
msgstr "OK"

#. i18n: ectx: property (text), widget (QPushButton, pbCancel)
#. i18n: ectx: property (text), widget (QPushButton, buttonCancel)
#: svncheckoutdialog.ui:81 svncleanupdialog.ui:96 svnprogressdialog.ui:33
#, kde-format
msgid "Cancel"
msgstr "Zrušiť"

#: svncleanupdialog.cpp:26
#, kde-format
msgctxt "@title:window"
msgid "Choose a directory to clean up"
msgstr "Vyberte priečinok na vyčistenie"

#: svncleanupdialog.cpp:60
#, kde-format
msgctxt "@info:status"
msgid "SVN clean up completed successfully."
msgstr "Čistenie skončilo úspešne."

#: svncleanupdialog.cpp:62
#, kde-format
msgctxt "@info:status"
msgid "SVN clean up failed for %1"
msgstr "Čistenie SVN zlyhalo pre %1"

#. i18n: ectx: property (windowTitle), widget (QWidget, SvnCleanupDialog)
#: svncleanupdialog.ui:20
#, kde-format
msgid "SVN Cleanup..."
msgstr "Vyčistiť SVN..."

#. i18n: ectx: property (text), widget (QLabel, label)
#: svncleanupdialog.ui:26
#, kde-format
msgid "Clean up directory:"
msgstr "Vyčistiť priečinok"

#. i18n: ectx: property (text), widget (QCheckBox, checkBoxUnversioned)
#: svncleanupdialog.ui:43
#, kde-format
msgid "Delete unversioned files and directories"
msgstr "Vymazať neverziované súbory a priečinky"

#. i18n: ectx: property (text), widget (QCheckBox, checkBoxIgnored)
#: svncleanupdialog.ui:50
#, kde-format
msgid "Delete ignored files and directories"
msgstr "Vymazať ignorované súbory a priečinky"

#. i18n: ectx: property (text), widget (QCheckBox, checkBoxExternals)
#: svncleanupdialog.ui:57
#, kde-format
msgid "Include externals"
msgstr "Zahrnúť externé"

#: svncommitdialog.cpp:88
#, kde-format
msgctxt "@label"
msgid "Description:"
msgstr "Popis:"

#: svncommitdialog.cpp:103
#, kde-format
msgctxt "@action:button"
msgid "Refresh"
msgstr "Obnoviť"

#: svncommitdialog.cpp:108
#, kde-format
msgctxt "@action:button"
msgid "Commit"
msgstr "Začleniť"

#: svncommitdialog.cpp:114
#, kde-format
msgctxt "@item:inmenu"
msgid "Revert"
msgstr "Vrátiť späť"

#: svncommitdialog.cpp:121
#, kde-format
msgctxt "@item:inmenu"
msgid "Show changes"
msgstr "Zobraziť zmeny"

#: svncommitdialog.cpp:128
#, kde-format
msgctxt "@item:inmenu"
msgid "Add file"
msgstr "Pridať súbor"

#: svncommitdialog.cpp:156
#, kde-format
msgctxt "@title:column"
msgid "Path"
msgstr "Cesta"

#: svncommitdialog.cpp:157
#, kde-format
msgctxt "@title:column"
msgid "Status"
msgstr "Stav"

#: svncommitdialog.cpp:210
#, kde-format
msgctxt "@item:intable"
msgid "Unversioned"
msgstr "Bez verzie"

#: svncommitdialog.cpp:213
#, kde-format
msgctxt "@item:intable"
msgid "Modified"
msgstr "Upravené"

#: svncommitdialog.cpp:216
#, kde-format
msgctxt "@item:intable"
msgid "Added"
msgstr "Pridané"

#: svncommitdialog.cpp:219
#, kde-format
msgctxt "@item:intable"
msgid "Deleted"
msgstr "Odstránené"

#: svncommitdialog.cpp:222
#, kde-format
msgctxt "@item:intable"
msgid "Conflict"
msgstr "V konflikte"

#: svncommitdialog.cpp:225
#, kde-format
msgctxt "@item:intable"
msgid "Missing"
msgstr "Chýbajúce"

#: svncommitdialog.cpp:228
#, kde-format
msgctxt "@item:intable"
msgid "Update required"
msgstr "Vyžadované aktualizácia"

#: svnlogdialog.cpp:94
#, kde-format
msgid "Update to revision"
msgstr "Aktualizovať na revíziu"

#: svnlogdialog.cpp:98 svnlogdialog.cpp:116
#, kde-format
msgid "Revert to revision"
msgstr "Vrátiť na verziu"

#: svnlogdialog.cpp:102
#, kde-format
msgid "Show changes"
msgstr "Zobraziť zmeny"

#: svnlogdialog.cpp:109
#, kde-format
msgid "Changes against working copy"
msgstr "Zmeny voči pracovnej kópii"

#: svnlogdialog.cpp:271
#, kde-format
msgctxt "@info:status"
msgid "SVN log: update to revision failed."
msgstr "Záznam SVN: aktualizácia na revíziu zlyhala."

#: svnlogdialog.cpp:273
#, kde-format
msgctxt "@info:status"
msgid "SVN log: update to revision %1 successful."
msgstr "Záznam SVN: aktualizácia na revíziu %1 bola úspešná."

#: svnlogdialog.cpp:283 svnlogdialog.cpp:294
#, kde-format
msgctxt "@info:status"
msgid "SVN log: revert to revision failed."
msgstr "Záznam SVN: vrátenie na revíziu zlyhalo."

#: svnlogdialog.cpp:285 svnlogdialog.cpp:296
#, kde-format
msgctxt "@info:status"
msgid "SVN log: revert to revision %1 successful."
msgstr "Záznam SVN: vrátenie na revíziu %1 bolo úspešné."

#. i18n: ectx: property (windowTitle), widget (QWidget, SvnLogDialog)
#: svnlogdialog.ui:20
#, kde-format
msgid "SVN Log"
msgstr "Záznam SVN"

#. i18n: ectx: property (text), widget (QTableWidget, tLog)
#: svnlogdialog.ui:55
#, kde-format
msgid "Revision"
msgstr "Verzia"

#. i18n: ectx: property (text), widget (QTableWidget, tLog)
#: svnlogdialog.ui:60
#, kde-format
msgid "Author"
msgstr "Autor"

#. i18n: ectx: property (text), widget (QTableWidget, tLog)
#: svnlogdialog.ui:65
#, kde-format
msgid "Date"
msgstr "Dátum"

#. i18n: ectx: property (text), widget (QTableWidget, tLog)
#: svnlogdialog.ui:70
#, kde-format
msgid "Message"
msgstr "Správa"

#. i18n: ectx: property (text), widget (QPushButton, pbNext100)
#: svnlogdialog.ui:104
#, kde-format
msgid "Next 100"
msgstr "Ďalších 100"

#. i18n: ectx: property (text), widget (QPushButton, pbRefresh)
#: svnlogdialog.ui:114
#, kde-format
msgid "Refresh"
msgstr "Obnoviť"

#: svnprogressdialog.cpp:53
#, kde-format
msgctxt "@info:status"
msgid "Error starting: %1"
msgstr "Chyba začína: %1"
